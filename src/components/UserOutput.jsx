import React from 'react'
import Radium from 'radium'

const UserOutput = (props) => {
  const style = {
    width: '90%',
    margin: '10px auto',
    padding: '10px',
    border: '2px solid red',
    borderRadius: '4px',
    '@media (max-width: 500px)': {
      width: '50%'
    }
  }
  return (
    <div style={style} onClick={props.onclick}>
      <p>{props.name}</p>
      <p>{props.age}</p>
    </div>
  )
}

export default Radium(UserOutput)