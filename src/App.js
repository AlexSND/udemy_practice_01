import React, { Component } from 'react';
import './App.css';
import Radium, { StyleRoot } from 'radium'
import UserOutput from './components/UserOutput';
import UserInput from './components/UserInput';

class App extends Component {
  state = {
    persons: [
      {
        id: 1,
        name: 'Alex',
        age: 31
      },
      {
        id: 2,
        name: 'John',
        age: 28
      },
      {
        id: 3,
        name: 'Jack',
        age: 33
      }
    ],
    showPeople: false
  }

  deletePersonHandler = (personIndex) => {
    const newPersons = [...this.state.persons]
    newPersons.splice(personIndex, 1)
    this.setState({
      persons: newPersons
    })
  }

  toggleHandler = () => {
    this.setState({
      showPeople: !this.state.showPeople
    })
  }

  render() {
    const buttonStyle = {
      padding: '10px',
      color: 'white',
      backgroundColor: 'green',
      marginBottom: '25px',
      outline: 'none',
      transition: '0.2s',
      ':hover': {
        backgroundColor: 'lightgreen'
      }
    }

    if(this.state.showPeople) {
      buttonStyle.backgroundColor = 'red'
      buttonStyle[':hover'] = {
        backgroundColor: 'salmon'
      }
    }

    const UserOutputClasses = []
    if(this.state.persons.length <= 2) {
      UserOutputClasses.push('red')
    }
    if(this.state.persons.length <= 1) {
      UserOutputClasses.push('bold')
    }

    const people = this.state.showPeople
      && this.state.persons.map((person, index) => {
      return  <UserOutput
                key={person.id}
                name={person.name}
                age={person.age}
                onclick={() => this.deletePersonHandler(index)} />
    })

    return (
      <StyleRoot>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <button style={buttonStyle} onClick={this.toggleHandler}>Show peoples</button>
          <p className={UserOutputClasses.join(' ')}>{this.state.persons.length}</p>
            {people}
        </div>
      </StyleRoot>
    );
  }
}

export default Radium(App);
